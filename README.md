### **Tareas a definir con Xavi**

**Versión 0.1**

descargar el plan comercial (listado de productos con objetivos) (excel)

1.  **Convertir en front o en back - 5hs (si me toca a mi)**

**Versión 0.5**

Envio de plan comercial para confirmacion, en vez de mandarlo por mis documentos. Pero conservar la accion de descargar el plan en excel

1.  **Back (modelado de datos, programado de metodos) (falta definir que me toca)**

**Versión 1**

- Creacion de plan de generacion de demandas con elección de tareas (no esta definido si se elige entre ya creadas, o va a haber un abm de tareas) feature nueva (back y front nuevos). Estas tareas pueden estar separadas por sucursales. Asignacion de puntajes a esas tareas. Esas tareas pueden estar relacionadas con las familias de productos
    1.  **Backend nuevo (a definir que tareas me tocan a mi)**
- Flujo de confirmacion de planes de demandas (con idas y vueltas)
    1.  **Backend de las features nuevas (a definir que tareas me tocan a mi)**
- Realizacion de esas tareas; comprobación de cumplimiento (links, fotos, etc)
    1.  **Backend de las features nuevas (a definir que tareas me tocan a mi)**
- (nueva pantalla) Creacion de tareas y asignacion de puntos en el backoffice del simulador (falta confirmar)
    1.  **ABM backend (a definir que tareas me tocan a mi)**
- Asignacion de bonificacion de descuentos dependiendo el puntaje registrado. A partir de los puntos que se cumplieron, cuanto porcentaje se aplica a tu plan. (se pone en el simulador o en la generacion de planes comerciales? a que afecta?)
    1.  **ABM backend (a definir que tareas me tocan a mi)**